﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace DynDns
{
	class Program
	{
		private static string logFileLocation = AppDomain.CurrentDomain.BaseDirectory;
		private static string logFileName = "DynDns.log";
		static void Main(string[] args)
		{
			var configFileLogLocation = ConfigurationManager.AppSettings["LogLocation"];
			if (!string.IsNullOrEmpty(configFileLogLocation))
				logFileLocation = configFileLogLocation;

			var setups = ConfigurationManager.AppSettings.AllKeys.Where(x => x.StartsWith("Host"));
			foreach (var setup in setups)
			{
				var config = ConfigurationManager.AppSettings[setup].Split('|');
				var provider = "";
				var user = "";
				var pass = "";
				var hosts = new List<string>();
				for (int i = 0; i < config.Length; i++)
				{
					switch (i)
					{
						case 0:
							provider = config[i];
							break;
						case 1:
							user = config[i];
							break;
						case 2:
							pass = config[i];
							break;
						default:
							hosts.Add(config[i]);
							break;
					}
				}

				UpdateHost(provider, user, pass, hosts);
			}
			Environment.Exit(0);
		}

		public static void UpdateHost(string provider, string username, string password, List<string> hosts)
		{
			var webClient = new WebClient
			{
				UseDefaultCredentials = true
			};

			if (string.IsNullOrEmpty(username))
			{
				WriteLog("UserName missing from config");
				return;
			}
			if (string.IsNullOrEmpty(password))
			{
				WriteLog("ZoneEditApiToken missing from config");
				return;
			}

			webClient.Credentials = new NetworkCredential(username, password);

			var hadErrror = false;
			var first = true;
			foreach (var host in hosts)
			{

				// Hurry up and wait.
				if (first)
				{
					first = false;
				}
				else
				{
					WriteLog("We have to wait 600 seconds between calls.");
					System.Threading.Thread.Sleep(new TimeSpan(0,0,0,610));
					WriteLog("Wait over.");
				}

				WriteLog($"updating the IP for {host}");
				try
				{
					var url = $"https://dynamic.zoneedit.com/auth/dynamic.html?host={host}";
					WriteLog($"Do get on: {url}");
					var result = webClient.DownloadString(url);
					WriteLog(result);
				}
				catch (Exception e)
				{
					hadErrror = true;
					WriteLog(e.Message);
				}
			}

			if (hadErrror)
			{
				//TODO: Set exit code here.
			}
		}

		private static String WildCardToRegular(String value)
		{
			return "^" + Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*") + "$";
		}

		public static void WriteLog(string msg)
		{
			var file = $"{logFileLocation}\\{DateTime.Now:yyyy-MM-dd}_{logFileName}";

			var msgWithDate = $"{DateTime.Now}: {msg}";
			Console.WriteLine(msgWithDate);
			using (StreamWriter sw = File.AppendText(file))
			{
				sw.WriteLine(msgWithDate);
			}
		}
	}
}
